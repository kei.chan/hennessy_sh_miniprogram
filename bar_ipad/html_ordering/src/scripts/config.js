import Vue from 'vue'

export const CONFIG = new Vue({
    data: {
        user: {},
        venue: "", // shanghai/malaysia
        stand: "", // A/B
        api_url: {
            shanghai: {
                api_prefix: 'http://origin-declassified.hennessy.com',
                //api_prefix: 'http://digital.tbwa.com.cn',
            },
            malaysia: {
                api_prefix: 'http://chatbot-my.hdeclassified.com',
                //api_prefix: 'http://hennessy-my.hennessy.com',
            },
            hongkong: {
                api_prefix: 'http://hk.hdeclassified.com',
                //api_prefix: 'http://hennessy-hk.hennessy.com',
            }
        },
        drinks: {
            shanghai: [
                {
                    name: 'Classivm Espresso',
                    drinkId: 'classivm_expresso',
                    image: require('@/assets/drinks/shanghai/classivm_expresso.jpg'),
                    enlarge_image: require('@/assets/drinks/shanghai/classivm_expresso.jpg'),
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: 'CP milkshake',
                    drinkId: 'cp_milkshake',
                    image: require('@/assets/drinks/shanghai/cp_milkshake.jpg'),
                    enlarge_image: require('@/assets/drinks/shanghai/cp_milkshake.jpg'),
                    type: 'milkshake',
                    startDate: 20181128,
                    endDate: 20181202,
                },{
                    name: 'Hennessy Sunset',
                    drinkId: 'hennessy_sunset',
                    image: require('@/assets/drinks/shanghai/hennessy_sunset.jpg'),
                    enlarge_image: require('@/assets/drinks/shanghai/hennessy_sunset.jpg'),
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: 'Hennessy X Cola',
                    drinkId: 'hennessy_x_cola',
                    image: require('@/assets/drinks/shanghai/hennessy_x_cola.jpg'),
                    enlarge_image: require('@/assets/drinks/shanghai/hennessy_x_cola.jpg'),
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: 'Shanghai Cobbler',
                    drinkId: 'shanghai_cobbler',
                    image: require('@/assets/drinks/shanghai/shanghai_cobbler.jpg'),
                    enlarge_image: require('@/assets/drinks/shanghai/shanghai_cobbler.jpg'),
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: 'V.S.O.P Ice-cream',
                    drinkId: 'vsop_ice_cream',
                    image: require('@/assets/drinks/shanghai/vsop_ice_cream.jpg'),
                    enlarge_image: require('@/assets/drinks/shanghai/vsop_ice_cream.jpg'),
                    type: 'icecream1',
                    startDate: 20181124,
                    endDate: 20181127,
                },{
                    name: 'X.O. Ice-cream',
                    drinkId: 'xo_ice_cream',
                    image: require('@/assets/drinks/shanghai/xo_ice_cream.jpg'),
                    enlarge_image: require('@/assets/drinks/shanghai/xo_ice_cream.jpg'),
                    type: 'icecream2',
                    startDate: 20181124,
                    endDate: 20181127,
                }
            ],
            malaysia: [
                {
                    name: 'Old Fashioned',
                    drinkId: "drink1",
                    image: require('@/assets/drinks/old_fashioned.jpg'),
                    type: 'local',
                    startDate: 20181101,
                    endDate: 20181210,
                },{
                    name: 'Birds Bees and the Trees',
                    drinkId: "drink2",
                    image: require('@/assets/drinks/int/birds_bees_and_the_trees.jpg'),
                    type: 'international',
                    startDate: 20181101,
                    endDate: 20181210,
                },{
                    name: 'Sunset Lime',
                    drinkId: "drink3",
                    image: require('@/assets/drinks/sunset_lime.jpg'),
                    type: 'local',
                    startDate: 20181101,
                    endDate: 20181210,
                },{
                    name: 'Chinatown',
                    drinkId: "drink4",
                    image: require('@/assets/drinks/int/chinatown.jpg'),
                    type: 'international',
                    startDate: 20181101,
                    endDate: 20181210,
                },{
                    name: 'Dark Cacao',
                    drinkId: "drink5",
                    image: require('@/assets/drinks/int/dark_cacao.jpg'),
                    type: 'international',
                    startDate: 20181101,
                    endDate: 20181210,
                },{
                    name: 'Sunset Ginger',
                    drinkId: "drink6",
                    image: require('@/assets/drinks/sunset_ginger.jpg'),
                    type: 'local',
                    startDate: 20181101,
                    endDate: 20181210,
                },{
                    name: 'Popping Grenade',
                    drinkId: "drink7",
                    image: require('@/assets/drinks/int/popping_grenade.jpg'),
                    type: 'international',
                    startDate: 20181101,
                    endDate: 20181210,
                },{
                    name: 'Sunset Special',
                    drinkId: "drink8",
                    image: require('@/assets/drinks/sunset_special.jpg'),
                    type: 'local',
                    startDate: 20181101,
                    endDate: 20181210,
                }
            ],
        }
    }
})