import Vue from 'vue'
import Router from 'vue-router'
//pages
import Config from '@/components/pages/Config'
import Welcome from '@/components/pages/Welcome'
import Select_Shanghai from '@/components/pages/Select-shanghai'
import Select_Malaysia from '@/components/pages/Select-malaysia'
import Success from '@/components/pages/Success'
import Redeemed from '@/components/pages/Redeemed'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import {CONFIG} from '@/scripts/config.js'

Vue.prototype.$logoimg = require('../assets/logo.png')
Vue.prototype.$axios = require('axios')
Vue.prototype.$config = CONFIG;

Vue.use(Router)
Vue.use(VueAwesomeSwiper, /* { default global options } */)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },{
      path: '/config',
      name: 'Config',
      component: Config
    },{
      path: '/select-shanghai',
      name: 'Select_Shanghai',
      component: Select_Shanghai
    },{
      path: '/select-malaysia',
      name: 'Select_Malaysia',
      component: Select_Malaysia
    },{
      path: '/success',
      name: 'Success',
      component: Success
    },{
      path: '/redeemed',
      name: 'Redeemed',
      component: Redeemed
    }
  ]
})
