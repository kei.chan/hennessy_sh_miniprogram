var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  socket.on('order completed', function(orderNumber){
    io.emit('order completed', orderNumber);
  });
  socket.on('order delivered', function(orderNumber){
    io.emit('order delivered', orderNumber);
  });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});

