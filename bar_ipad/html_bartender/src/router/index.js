import Vue from 'vue'
import Router from 'vue-router'
import Listing from '@/components/pages/listing.vue'
import Config from '@/components/pages/config.vue'
import {CONFIG} from '@/scripts/config.js'

Vue.prototype.$axios = require('axios')
Vue.prototype.$config = CONFIG;

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Listing',
      component: Listing
    },{
      path: '/listing',
      name: 'Listing',
      component: Listing
    },{
      path: '/config',
      name: 'Config',
      component: Config
    }
  ]
})
