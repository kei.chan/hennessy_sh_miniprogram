import Vue from 'vue'

export const CONFIG = new Vue({
    data: {
        role: "",
        socket_port: "http://kei-dev.shinggor.com:3000",
        api_url: {
            shanghai: {
                api_prefix: 'http://origin-declassified.hennessy.com',
                //api_prefix: 'http://digital.tbwa.com.cn',
            },
            malaysia: {
                api_prefix: 'http://chatbot-my.hdeclassified.com',
                //api_prefix: 'http://hennessy-my.hennessy.com',
            },
            hongkong: {
                api_prefix: 'http://hk.hdeclassified.com',
                //api_prefix: 'http://hennessy-hk.hennessy.com',
            }
        },
        drinks: {
            shanghai: [
                {
                    name: '大潮咖',
                    drinkId: 'classivm_expresso',
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: '粉新CP奶昔',
                    drinkId: 'cp_milkshake',
                    type: 'milkshake',
                    startDate: 20181128,
                    endDate: 20181202,
                },{
                    name: '时光变奏',
                    drinkId: 'hennessy_sunset',
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: '幸福里',
                    drinkId: 'hennessy_x_cola',
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: '吴依软语',
                    drinkId: 'shanghai_cobbler',
                    type: 'cocktail',
                    startDate: 20181124,
                    endDate: 20181202,
                },{
                    name: 'V.S.O.P 香草冰淇淋',
                    drinkId: 'vsop_ice_cream',
                    type: 'icecream',
                    startDate: 20181124,
                    endDate: 20181127,
                },{
                    name: 'X.O. 巧克力冰淇淋',
                    drinkId: 'xo_ice_cream',
                    type: 'icecream',
                    startDate: 20181124,
                    endDate: 20181127,
                }
            ],
            malaysia: [
                {
                    name: 'Old Fashioned',
                    drinkId: "drink1",
                    type: 'local',
                },{
                    name: 'Birds Bees and the Trees',
                    drinkId: "drink2",
                    type: 'international',
                },{
                    name: 'Sunset Lime',
                    drinkId: "drink3",
                    type: 'local',
                },{
                    name: 'Chinatown',
                    drinkId: "drink4",
                    type: 'international',
                },{
                    name: 'Dark Cacao',
                    drinkId: "drink5",
                    type: 'international',
                },{
                    name: 'Sunset Ginger',
                    drinkId: "drink6",
                    type: 'local',
                },{
                    name: 'Popping Grenade',
                    drinkId: "drink7",
                    type: 'international',
                },{
                    name: 'Sunset Special',
                    drinkId: "drink8",
                    type: 'local',
                }
            ],
        }
    }
})