import Vue from 'vue'

export const CONFIG = new Vue({
    data: {
      userId : '',
      products: [
        {
            "_id" : "5beaf394cb518f5553058954",
            "type" : 0,
            "name" : "General Event",
            "zhname" : "解码轩尼诗"
        },{
            "_id" : "5beaf394cb518f5553058955",
            "type" : 1,
            "name" : "Mixology",
            "zhname" : "轩尼诗调酒艺术"
        },{
            "_id" : "5beaf394cb518f5553058956",
            "type" : 2,
            "name" : "Maturation",
            "zhname" : "轩尼诗陈酿的艺术"
        },{
            "_id" : "5beaf394cb518f5553058957",
            "type" : 3,
            "name" : "Meals",
            "zhname" : "轩尼诗美食工坊"
        }
      ],
      api_url: {
        shanghai: {
            api_prefix: 'http://origin-declassified.hennessy.com',
            //api_prefix: 'http://digital.tbwa.com.cn',
        },
        malaysia: {
            api_prefix: 'http://chatbot-my.hdeclassified.com',
            //api_prefix: 'http://hennessy-my.hennessy.com',
        },
        hongkong: {
            api_prefix: 'http://hk.hdeclassified.com',
            //api_prefix: 'http://hennessy-hk.hennessy.com',
        }
      },
    }
})