import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/pages/index.vue'
import Ticket from '@/components/pages/ticket.vue'
import Success from '@/components/pages/success.vue'
import Fail from '@/components/pages/fail.vue'
import Config from '@/components/pages/config.vue'
import {CONFIG} from '@/scripts/config.js'

Vue.prototype.$logoimg = require('../assets/logo.png')
Vue.prototype.$axios = require('axios')
Vue.prototype.$config = CONFIG

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },{
      path: '/welcome',
      name: 'Welcome',
      component: Welcome
    },{
      path: '/ticket',
      name: 'Ticket',
      component: Ticket
    },{
      path: '/success',
      name: 'Success',
      component: Success
    },{
      path: '/fail',
      name: 'Fail',
      component: Fail
    },{
      path: '/config',
      name: 'Config',
      component: Config
    }
  ]
})
