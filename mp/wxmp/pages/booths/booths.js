// pages/booths/booths.js
const app = getApp()
const CONFIG = app.globalData.configs

Page({

  data: {
    userInfo: {},
    uuid: CONFIG.uuid,
    validCount: 0,
    invalidCount: 0,
    validCurrent: '',
    groups: [
      ['14681', '14682', '36312'],
      ['36313', '36314', '36315']
    ],
    beacons_raw: [],
    beacons: [],
    beaconGroups: [],
    booths: app.globalData.booths,
    currentBooth: app.globalData.currentBooth,
    toView: "",
  },

  onLoad: function (options) {
    this.setUserInfo()
    this.startBeaconDiscovery();
  },
  
  onReady: function () {

  },
  
  onShow: function () {
    var vm = this;
    app.globalData.isBoothHiding = false;

    //scroll to current booth
    vm.setData({
      currentBooth: app.globalData.currentBooth,
    })
    if (app.globalData.currentBooth != -1){
      vm.setData({
        toView: 'booth-' + app.globalData.currentBooth,
      })
    }

    //activate shake function
    vm.isShow = true;
    wx.onAccelerometerChange(function (e) {
      if (!vm.isShow) {
        return
      }
      //console.log(e.x);
      //console.log(e.y);
      if (e.x > 1 && e.y > 1) {
        /*wx.showToast({
          title: '摇一摇成功',
          icon: 'success',
          duration: 2000
        })*/
        //if (app.globalData.listenShake == true && vm.data.currentBooth != -1) {
          //vm.gotoCamera();
        if (app.globalData.listenShake == true ) {
          console.log('shaked');
          vm.unlockBooth();
        }
      }
    })
    vm.setData({
      booths: app.globalData.booths,
    })
  },

  onHide: function () {
    this.isShow = false;
    app.globalData.isBoothHiding = true;
  },
  
  onUnload: function () {
    this.stopBeaconDiscovery();
  },
  
  onPullDownRefresh: function () {

  },
  
  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  },

  startBeaconDiscovery: function(){
    var vm = this;
    wx.startBeaconDiscovery({
      uuids: ['FDA50693-A4E2-4FB1-AFCF-C6EB07647825'],
      success: function (res) {
        wx.onBeaconUpdate(function(res1){ //return arr
          var beacons = res1.beacons;
          var allBeacons = [].concat(vm.data.beacons);
          
          for(var i=0; i<beacons.length; i++){
            var thizBeacon = beacons[i];

            var checkArr = allBeacons.filter(beacon => beacon.minor == thizBeacon.minor);
            
            if (checkArr.length > 0 ){
              //beacons existed, extend the beacon with new value
              var idx = allBeacons.indexOf(checkArr[0]);
              var obj = thizBeacon;
              allBeacons[idx] = obj;
              vm.setData({
                beacons: allBeacons
              })
            }else{
              //beacons NOT existed, push the beacon
              allBeacons.push(thizBeacon);
              vm.setData({
                beacons: allBeacons
              })
            }

          }

          vm.setBeaconGroups(vm.data.beacons)
        })
       },
      fail: function (res) {

       },
      complete: function (res) {

       },
    })
  },
  stopBeaconDiscovery: function(){
    var vm = this;
    wx.stopBeaconDiscovery({})
  },
  gotoCamera: function () {
    var vm = this;
    var query = '?boothName=' + vm.data.currentBooth;
    wx.redirectTo({
      url: '/pages/camera/camera' + query,
      success: function () {
        app.globalData.listenShake = false;
      }
    })
  },

  unlockBooth: function () {
    var vm = this;
    var beaconGroups = vm.data.beaconGroups;

    

    for (var i = 0; i < beaconGroups.length; i++) {
      var group = beaconGroups[i];
      var beacons = group.beacons;
      var arr = beacons.filter(beacon => beacon.accuracy < CONFIG.activeDistance); //valid devices
      if (arr.length >= CONFIG.activeDeviceCount && arr.length >= beacons.length * 0.5) { //more than 50% devices passed

        app.globalData.currentBooth = group.groupName;
        vm.setData({
          currentBooth: app.globalData.currentBooth,
        });

        var currentBooth = app.globalData.booths.filter(booth => booth.name == group.groupName)[0];
        var currentBooth_idx = app.globalData.booths.indexOf(currentBooth);

        if (app.globalData.booths[currentBooth_idx].checked == false ){
          //ui
          vm.setData({
            toView: 'booth-' + vm.data.currentBooth,
          })
          wx.vibrateLong();
          wx.showToast({
            title: '成功解鎖',
            icon: 'success',
            duration: 3000,
            image: '../../images/declassified_logo_en.png'
          })
        }
        app.globalData.booths[currentBooth_idx].checked = true;
        vm.setData({
          booths: app.globalData.booths,
        })

        return;
      }
    }

    app.globalData.currentBooth = -1;
    vm.setData({
      currentBooth: app.globalData.currentBooth,
    });

  },

  lockBooth: function() {

  },

  setCurrentBeacon: function (beaconGroups){
    var vm = this;
    for (var i = 0; i < beaconGroups.length; i++) {
      var group = beaconGroups[i];
      var beacons = group.beacons;
      var arr = beacons.filter(beacon => beacon.accuracy < CONFIG.activeDistance); //valid devices
      if (arr.length >= CONFIG.activeDeviceCount && arr.length >= beacons.length * 0.5) { //more than 50% devices passed
        if (vm.data.validCurrent != group.groupName) { //continously same
          vm.setData({
            validCurrent: group.groupName,
          })
          break;
        }
        //passed
        vm.setData({
          invalidCount: 0,
          validCurrent: group.groupName,
          validCount: vm.data.validCount + 1,
        })
        
        if (vm.data.validCount >= CONFIG.activeDuration) { //continously same
          var currentBooth = app.globalData.booths.filter(booth => booth.name == vm.data.validCurrent)[0];
          var currentBooth_idx = app.globalData.booths.indexOf(currentBooth);

          if (app.globalData.booths[currentBooth_idx].checked == true) {
            app.globalData.currentBooth = vm.data.validCurrent
            vm.setData({
              currentBooth: app.globalData.currentBooth,
            });
          }
          /*app.globalData.currentBooth = vm.data.validCurrent;
          vm.setData({
            currentBooth: app.globalData.currentBooth,
          });
          var currentBooth = app.globalData.booths.filter(booth => booth.name == vm.data.validCurrent)[0];
          var currentBooth_idx = app.globalData.booths.indexOf(currentBooth);
          if (app.globalData.booths[currentBooth_idx].checked == false){
            vm.setData({
              toView : 'booth-' + vm.data.validCurrent,
            })
            wx.vibrateLong();
            wx.showToast({
              title: '成功解鎖',
              icon: 'success',
              duration: 3000,
              image: '../../images/declassified_logo_en.png'
            })
          }
          app.globalData.booths[currentBooth_idx].checked = true;
          vm.setData({
            booths: app.globalData.booths,
          })*/
        }
        return;
      }
    }
    vm.setData({
      validCount: 0,
      invalidCount: vm.data.invalidCount - 1,
    });
    if (vm.data.invalidCount <= -5) {
      app.globalData.currentBooth = -1;
      vm.setData({
        currentBooth: app.globalData.currentBooth
      });
    }
  },
  setUserInfo: function () {
    var vm = this;
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        openid: app.globalData.openid,
        hasUserInfo: true
      })
      wx.setStorageSync('userInfo', app.globalData.userInfo);
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          openid: app.globalData.openid,
          hasUserInfo: true
        })
        wx.setStorageSync('userInfo', res.userInfo);
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            openid: app.globalData.openid,
            hasUserInfo: true
          })
          wx.setStorageSync('userInfo', res.userInfo);
        }
      })
    }
  },
  setBeaconGroups: function (beacons) {
    var vm = this;
    var beaconGroups = [];
    //regroup the individual beacons
    for (var i = 0; i < beacons.length; i++) {

      var beacon = beacons[i];
      var checkArr = beaconGroups.filter(group => group.groupMinors.indexOf(beacon.minor.toString()) >= 0);

      if (checkArr.length > 0) {
        //group existed
        var index = beaconGroups.indexOf(checkArr[0]);
        beaconGroups[index].beacons.push(beacon);
      } else {
        //not yet created group
        if (beacon.minor == 14681 || beacon.minor == 14682 || beacon.minor == 36312) {
          var obj = {
            groupName: app.globalData.booths[0].name,
            groupMinors: ['14681', '14682', '36312'],
            beacons: []
          };
          obj.beacons.push(beacon);
        }
        if (beacon.minor == 36313 || beacon.minor == 36314 || beacon.minor == 36315) {
          var obj = {
            groupName: app.globalData.booths[1].name,
            groupMinors: ['36313', '36314', '36315'],
            beacons: []
          };
          obj.beacons.push(beacon);
        }
        beaconGroups.push(obj);
      }
    }
    //extend the existing beacons group, since not every update included all the beacons
    vm.setData({
      beaconGroups: beaconGroups,
    })
    vm.setCurrentBeacon(beaconGroups);
  }
})