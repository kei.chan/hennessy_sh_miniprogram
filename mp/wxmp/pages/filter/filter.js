// pages/filter/filter.js
const app = getApp()

Page({

  /**
   * Page initial data
   */
  data: {
    source_photo: '',
    navigatortype: 'redirect',
    selectedFilter: '',
    currentBoothName: '',
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    var vm = this;
    var booths = app.globalData.booths;
    var booth = booths.filter(booth => booth.name == options.boothName)[0];
    this.setData({
      source_photo: booth.photo,
      currentBoothName: options.boothName,
    })

    if(booth.filter != ''){
      vm.drawCanvas(booth.filter);  
    }else{
      vm.drawCanvas();
    }

    if( app.globalData.isBoothHiding == true ){
      vm.setData({
        navigatortype: 'navigateBack',
      })
    }else{
      vm.setData({
        navigatortype: 'redirect',
      })
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },

  chooseFilter_1: function() {
    this.drawCanvas(0)
  },
  chooseFilter_2: function () {
    this.drawCanvas(1)
  },
  chooseFilter_3: function () {
    this.drawCanvas(2)
  },

  drawCanvas: function(filter_idx) {
    var vm = this;
    var context = wx.createCanvasContext('filterCanvas')
    var images = vm.data.source_photo;
    var width = wx.getSystemInfoSync().windowWidth;
    var filters = [
      "../../images/filter-1.png",
      "../../images/filter-2.png",
      "../../images/filter-3.png",
    ]

    if (!filter_idx){
      filter_idx = 0;
    }

    vm.setData({
      selectedFilter: filter_idx,
    })

    context.drawImage(images, 0, 0, width, width);
    context.drawImage(filters[filter_idx], 0, 0, width, width);
    context.draw()


    vm.setFilteredImage();

  },

  setFilteredImage: function(imgurl){
    var vm = this;

    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: wx.getSystemInfoSync().windowWidth,
      height: wx.getSystemInfoSync().windowWidth,
      canvasId: 'filterCanvas',
      success: function (res) {
        var img_uri = res.tempFilePath

        var booths = app.globalData.booths;
        var booth = booths.filter(booth => booth.name == vm.data.currentBoothName)[0];
        var booth_idx = booths.indexOf(booth);

        booths[booth_idx].filter = vm.data.selectedFilter;
        booths[booth_idx].filteredPhoto = img_uri;

      }
    })


  }

})