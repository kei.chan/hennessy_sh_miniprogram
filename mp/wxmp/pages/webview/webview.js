// pages/webview/webview.js
const app = getApp()

Page({

  /**
   * Page initial data
   */
  data: {
    url: "",
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {


    var obj = {
      nickName : app.globalData.userInfo.nickName,
      avatarUrl : app.globalData.userInfo.avatarUrl,
      openid: app.globalData.openid,
    }
    this.setData({
      url: 'http://digital.tbwa.com.cn/mp/?' + 'nickName=' + obj.nickName + '&avatarUrl=' + obj.avatarUrl + '&openid=' + obj.openid,
      //url: 'http://digital.tbwa.com.cn/mp/',
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  },

})