const app = getApp()

Page({
  onShareAppMessage() {
    return {
      title: 'camera',
      path: 'page/component/pages/camera/camera'
    }
  },
  data: {
    src: '',
    videoSrc: '',
    position: 'front',
    mode: 'scanCode',
    result: {},
    boothName: '',
  },
  onLoad(option) {
    this.ctx = wx.createCameraContext()
    this.setData({
      boothName: option.boothName,
    })
    console.log(option)
  },
  onUnload: function () {
    app.globalData.listenShake = true;
  },
  takePhoto() {
    var vm = this;
    this.ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        this.setData({
          src: res.tempImagePath
        })
        //set the photo taken as global variable
        var booths = app.globalData.booths;
        var booth = booths.filter(booth => booth.name == vm.data.boothName)[0];
        var idx = booths.indexOf(booth);
        app.globalData.booths[idx].photo = res.tempImagePath;
        /*wx.navigateBack({
          delta: 1
        })*/
        wx.redirectTo({
          url: '/pages/filter/filter?boothName=' + vm.data.boothName,
        })
      }
    })
  },
  startRecord() {
    this.ctx.startRecord({
      success: () => {
        console.log('startRecord')
      }
    })
  },
  stopRecord() {
    this.ctx.stopRecord({
      success: (res) => {
        this.setData({
          src: res.tempThumbPath,
          videoSrc: res.tempVideoPath
        })
      }
    })
  },
  togglePosition() {
    this.setData({
      position: this.data.position === 'front'
        ? 'back' : 'front'
    })
  },
  error(e) {
    console.log(e.detail)
  }
})
