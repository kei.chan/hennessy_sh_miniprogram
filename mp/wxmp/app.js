//app.js
App({
  onLaunch: function () {
    var vm = this;
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        var code = res.code

        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session?appid=' + vm.globalData.DEVELOP.appid + '&secret=' + vm.globalData.DEVELOP.secret + '&js_code=' + code + '&grant_type=authorization_code',
          data: {},
          method: 'GET',
          success: function (res) {
            console.log(res);
            var obj = {};
            obj.openid = res.data.openid;
            obj.expires_in = Date.now() + res.data.expires_in;
            obj.session_key = res.data.session_key;
            wx.setStorageSync('openid', obj.openid);// 存储openid  
            vm.globalData.openid = obj.openid;
          }
        });

      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },

  globalData: {
    userInfo: null,
    openid: '',
    listenShake: true,
    currentBooth: -1,
    DEVELOP: {
      appid: 'wx1f33bb343019e0d6',
      secret: '9d254ccf82dea520825e7e7bb2a57a04', 
      openid: "opZxN5bgOnC_GEpqFym5TmCUU_xw",
    },
    DAN: {
      appid: 'wxc9d6687423232381',
      secret: '0029fdf5d0f6ea5ab560044131fdab18',
    },
    booths: [
      {
        name: 'ENTRANCE01',
        visual: '../../images/booth1.jpg',
        keylearning: "知道什么是“粗酒”（brouillis）吗？ 通过双重蒸馏获得的清澈透明的液体，被称为“生命之水”（eaux-de - vie）。它们将被陈酿成品质非凡的干邑。",
        checked: false,
        idle: true,
        photo: '',
        filter: '',
        filteredPhoto: '',
      }, {
        name: 'bar01',
        visual: '../../images/booth2.jpg',
        keylearning: "有个不太靠谱却又一直盛行的说法，就是干邑在家存放久了，会自自然然酿成更佳的风味。所以，V.S.O.P放久了真的会变成X.O吗？",
        checked: false,
        idle: true,
        photo: '',
        filter: '',
        filteredPhoto: '',
      }
    ],
    configs: {
      uuid: 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825',
      activeDeviceCount: 1,
      activeDistance: 5, //in metres
      activeDuration: 5, //in ibeacon interval
      beacons: [
        {
          name: 'ENTRANCE01',
          clone_name: 'ENTRANCE01 - 1',
          device_id: 22603332,
          major: 10190,
          minor: 14681
        }, {
          name: 'ENTRANCE01',
          clone_name: 'ENTRANCE01 - 2',
          device_id: 22603334,
          major: 10190,
          minor: 14682
        }, {
          name: 'ENTRANCE01',
          clone_name: 'ENTRANCE01 - 3',
          device_id: 22626158,
          major: 10190,
          minor: 36312
        }, {
          name: 'bar01',
          clone_name: 'bar01 - 1',
          device_id: 22626160,
          major: 10190,
          minor: 36313
        }, {
          name: 'bar01',
          clone_name: 'bar01 - 2',
          device_id: 22626161,
          major: 10190,
          minor: 36314
        }, {
          name: 'bar01',
          clone_name: 'bar01 - 3',
          device_id: 22626162,
          major: 10190,
          minor: 36315
        }
      ],
    }
  }
})