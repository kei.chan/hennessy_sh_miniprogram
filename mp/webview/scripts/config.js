var CONFIG = {
    requestSignature: 'http://digital.tbwa.com.cn/declassified/zh-cn/api/getSignature',
    appId:  'wxc9d6687423232381', // (DAN)
    appSecret: '0029fdf5d0f6ea5ab560044131fdab18', // (DAN)
    //appId:  'wx1f33bb343019e0d6', // (KEI)
    //appSecret: '9d254ccf82dea520825e7e7bb2a57a04', // (KEI)
    activeDeviceCount: 1,
    activeDistance: 5, //in metres
    activeDuration: 5, //in ibeacon interval
    uuid: ['FDA50693-A4E2-4FB1-AFCF-C6EB07647825'],
    booths: [
        {
            name: 'ENTRANCE01',
            visual: './images/booth1.png',
            keylearning: "知道什么是“粗酒”（brouillis）吗？ 通过双重蒸馏获得的清澈透明的液体，被称为“生命之水”（eaux-de - vie）。它们将被陈酿成品质非凡的干邑。",
        },{
            name: 'bar01',
            visual: './images/booth2.png',
            keylearning: "有个不太靠谱却又一直盛行的说法，就是干邑在家存放久了，会自自然然酿成更佳的风味。所以，V.S.O.P放久了真的会变成X.O吗？",
        }
    ],
    beacons: [
        {
            name: 'ENTRANCE01',
            clone_name: 'ENTRANCE01 - 1',
            device_id: 22603332,
            uuid: 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825',
            major: 10190,
            minor: 14681
        },{
            name: 'ENTRANCE01',
            clone_name: 'ENTRANCE01 - 2',
            device_id: 22603334,
            uuid: 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825',
            major: 10190,
            minor: 14682
        },{
            name: 'ENTRANCE01',
            clone_name: 'ENTRANCE01 - 3',
            device_id: 22626158,
            uuid: 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825',
            major: 10190,
            minor: 36312
        },{
            name: 'bar01',
            clone_name: 'bar01 - 1',
            device_id: 22626160,
            uuid: 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825',
            major: 10190,
            minor: 36313
        },{
            name: 'bar01',
            clone_name: 'bar01 - 2',
            device_id: 22626161,
            uuid: 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825',
            major: 10190,
            minor: 36314
        },{
            name: 'bar01',
            clone_name: 'bar01 - 3',
            device_id: 22626162,
            uuid: 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825',
            major: 10190,
            minor: 36315
        }
    ]
}

var TESTING = {
    beacons_1: [
        {
            groupName: 'name1',
            beacons: [
                {accuracy: 1},
                {accuracy: 2},
                {accuracy: 3},
            ]
        },{
            groupName: 'name2',
            beacons: [
                {accuracy: 7},
                {accuracy: 6},
                {accuracy: 5},
            ]
        }
    ],
    beacons_2: [
        {
            groupName: 'name1',
            beacons: [
                {accuracy: 10},
                {accuracy: 2},
                {accuracy: 3},
            ]
        },{
            groupName: 'name2',
            beacons: [
                {accuracy: 6},
                {accuracy: 6},
                {accuracy: 6},
            ]
        }
    ]
}


/* disable point mechanics and quiz 
var POINTS = {
    total: 200,
    game: 20,
    question: 10,
    checkin: 10
}
var QUIZS = [
    {
        booth_minor: '14681',
        questions: [
            {
                title: "以下那個是軒尼詩的酒",
                options: [
                    "啤酒",
                    "香檳",
                    "干邑"
                ],
                selected: -1,
                answer: 2,
            },{
                title: "以下那個不是軒尼詩的酒",
                options: [
                    "XO",
                    "藍帶",
                    "VSOP"
                ],
                selected: -1,
                answer: 1,
            },
        ]
    },{
        booth_minor: '14682',
        questions: [
            {
                title: "以下那個是軒尼詩的酒",
                options: [
                    "啤酒",
                    "香檳",
                    "干邑"
                ],
                selected: -1,
                answer: 2,
            },{
                title: "以下那個不是軒尼詩的酒",
                options: [
                    "XO",
                    "藍帶",
                    "VSOP"
                ],
                selected: -1,
                answer: 1,
            },
        ]
    }
]
*/