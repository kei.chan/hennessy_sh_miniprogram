var app = new Vue({
    el: '#app',
    data: {
        userInfo: {},
        //totalPoints: POINTS.total,
        //POINTS: POINTS,
        ready: false,
        bluetoothReady: false,
        validCount: 0,
        invalidCount: 0,
        userData: {
            openid: '',
            //rfid: '',
            //points: 10,
            //quizs: QUIZS,
            booths: [{
                name:           CONFIG.booths[0].name,
                visual:         CONFIG.booths[0].visual,
                checkedIn:      false,
                keylearning:    CONFIG.booths[0].keylearning,
                //checkInPoints:  POINTS.checkin,
                /*game: {
                    name:           '遊戲1',
                    description:    '請與提子樹自拍並上傳，賺取分數',
                    points:         POINTS.game,
                    completed:      false,
                    value:          '',
                    gameType:       'photo',
                    selectedFilter: '',
                }*/
            }, {
                name:           CONFIG.booths[1].name,
                visual:         CONFIG.booths[1].visual,
                checkedIn:      false,
                keylearning:    CONFIG.booths[1].keylearning,
                //checkInPoints:  POINTS.checkin,
                /*game: {
                    name:           '遊戲2',
                    description:    '請替調酒師拍照，賺取分數',
                    points:         POINTS.game,
                    completed:      false,
                    value:          '',
                    gameType:       'photo',
                    selectedFilter: '',
                }*/
            }],
        },
        beacons: [],
        beaconGroups: [],
        currentIbeacon: -1,
        popupCallback: function(){},
        beaconsInterval: function(){},
        motion: {
            //shakeThreshold: 1000,
            shakeThreshold: 2500,
            lastUpdate: 0,
            x: '',
            y: '',
            z: '',
            lastX: '',
            lastY: '',
            lastZ: '',
        },
        withinPage: true,
        /*gamesMethods: function(number){
            var vm = this;
            switch (number) {
                case 0:
                    game0()
                    break;
                case 1:
                    game1()
                    break;
            }
            function game0(){
                wxfn.chooseImage(0);
            }
            function game1(){
                wxfn.chooseImage(1);
            }
        },
        quizsMethods: function(number){
            var vm = this;
            switch (number) {
                case 0:
                    quiz0()
                    break;
                case 1:
                    quiz1()
                    break;
            }
            function quiz0(){
                $('#popup-quiz-0-question-0').lightbox_me();
            }
            function quiz1(){
                $('#popup-quiz-1-question-0').lightbox_me();
            }
        }*/
    },
    watch: {
        ready: function(){
            navigator.vibrate(200);
        },
        beacons: function(newBeacons){
            var vm = this;
            //var newBeacons = CONFIG.beacons;
            //vm.setBeaconGroups(newBeacons);
        },
        currentIbeacon: function(newBeaconName){
            this.gotoCurrentBeacon(newBeaconName)
            /*switch (newBeaconName) {
                case 'ENTRANCE01':
                    break;
                case 'bar01':
                    break;
            }*/
            //this.addCheckinPoints(newBeaconName);
        },
        /*lvTitle: function(newTitle){
            //$.featherlight('#popup-lv');
            //$('#popup-lv').lightbox_me();
            var vm = this;
            vm.popupCallback = function(){
                $('#popup-lv').lightbox_me({
                    onClose: function(){
                        vm.popupCallback = function(){};
                    }
                });
            }
        }*/
    },
    computed: {
        /*barWidth: function(){
            var str = ((this.userData.points / this.totalPoints)*100).toString() + '%';
            return str;
        },
        level: function(){
        },
        lvTitle: function(){
            var value = this.userData.points / this.totalPoints;
            var str = 'hennessy beginner';
            if(value > 0.1 && value < 0.4){
                str = 'hennessy advance'
            }else if(value >= 0.4 & value < 0.8){
                str = 'hennessy profession'
            }else if(value >= 0.8){
                str = 'hennessy insider'
            }
            return str;
        }*/        
    },
    methods: {
        manualBeaconsInterval: function(){
            var vm = this;
            vm.beaconsInterval = setInterval(function(){
                vm.setBeaconGroups(vm.beacons);
            },1000);
        },
        setBeaconGroups: function(beacons){
            var vm = this;
            var groups_arr = [];

            for (var i = 0; i < beacons.length; i++){

                var beacon = beacons[i];
                var checkArr = groups_arr.filter(group => group.groupMinors.indexOf(beacon.minor) >= 0);

                if( checkArr.length > 0 ){
                    //group existed
                    var index = groups_arr.indexOf(checkArr[0]);
                    groups_arr[index].beacons.push(beacon);
                }else{
                    //not yet created group
                    if(beacon.minor == 14681 || beacon.minor == 14682 || beacon.minor == 36312){
                        var obj = {
                            groupName: CONFIG.booths[0].name,
                            groupMinors: ['14681', '14682', '36312'],
                            beacons: []
                        };
                        obj.beacons.push(beacon);
                    }
                    if(beacon.minor == 36313 || beacon.minor == 36314 || beacon.minor == 36315){
                        var obj = {
                            groupName: CONFIG.booths[1].name,
                            groupMinors: ['36313', '36314', '36315'],
                            beacons: []
                        };
                        obj.beacons.push(beacon);
                    }
                    groups_arr.push(obj);
                }
            }


            vm.beaconGroups = groups_arr;
            this.setCurrentBeacon(groups_arr);
        },
        setCurrentBeacon: function(beaconGroups){
            var vm = this;
            grouploop:
            for( var i=0; i<beaconGroups.length; i++ ){
                var group = beaconGroups[i];
                var beacons = group.beacons;
                var arr = beacons.filter(beacon => beacon.accuracy < CONFIG.activeDistance); //valid devices
                //if( arr.length >= CONFIG.activeDeviceCount && arr.length == beacons.length ){
                if( arr.length >= CONFIG.activeDeviceCount && arr.length >= beacons.length*0.5 ){
                    if(vm.validCurrent != group.groupName){
                        vm.validCurrent = group.groupName;
                        break;
                    }
                    //passed
                    vm.invalidCount = 0;
                    vm.validCurrent = group.groupName;
                    vm.validCount = vm.validCount + 1;
                    if(vm.validCount >= CONFIG.activeDuration ){
                        vm.currentIbeacon = vm.validCurrent;
                    }
                    return;
                }
            }
            vm.validCount = 0;
            vm.invalidCount = vm.invalidCount - 1;
            if( vm.invalidCount <= -5 ){
                vm.currentIbeacon = -1;
            }
        },
        setUserInfo: function(){
            var vm = this;
            var obj = {
                nickName: getUrlParameter('nickName'),
                avatarUrl: getUrlParameter('avatarUrl'),
            }
            vm.userInfo = obj;
            vm.userData.openid = getUrlParameter('openid');
        },
        gotoCurrentBeacon: function(beaconName){
            var vm = this;
            var arr = vm.userData.booths;
            var obj = arr.filter(booth => booth.name == beaconName)[0];
            var idx = arr.indexOf(obj);
            if(arr[idx].checkedIn != true){ //scroll to the booth ONLY WHEN first time unlock
                arr[idx].checkedIn = true;
                navigator.vibrate(1000);
                var _id = "booth-"+idx;
                var elm = document.getElementById(_id);
                elm.scrollIntoView({
                    behavior: "smooth"
                });
            }
        }, 
        deviceMotionHandler: function(eventData){
            var vm = this;
            var acceleration = eventData.accelerationIncludingGravity; // 获取含重力的加速度
            var curTime = new Date().getTime();
            // 100毫秒进行一次位置判断
            if(vm.withinPage == false){return;}
            if ((curTime - vm.motion.lastUpdate) > 100) {
                var diffTime = curTime - vm.motion.lastUpdate;
                vm.motion.lastUpdate = curTime;
                vm.motion.x = acceleration.x;
                vm.motion.y = acceleration.y;
                vm.motion.z = acceleration.z;
                var speed = Math.abs(vm.motion.x + vm.motion.y + vm.motion.z - vm.motion.lastX - vm.motion.lastY - vm.motion.lastZ) / diffTime * 10000;
                // 前后x, y, z间的差值的绝对值和时间比率超过了预设的阈值，则判断设备进行了摇晃操作
                if (speed > vm.motion.shakeThreshold) {
                    //alert('搖一搖成功')
                    wxfn.navigateTo({
                        url: '../camera/camera?boothName=ENTRANCE01',
                        success: function(){
                            vm.withinPage = false;
                        }
                    })
                }
                vm.motion.lastX = vm.motion.x;
                vm.motion.lastY = vm.motion.y;
                vm.motion.lastZ = vm.motion.z;
            }
        },
        activateShakeFn: function(){
            var vm = this;
            // 监听传感器运动事件
            if (window.DeviceMotionEvent) {
                window.addEventListener('devicemotion', vm.deviceMotionHandler, false);
            } else {
                alert('本设备不支持devicemotion事件');
            }
        }, 
        deactivateShakeFn: function(){
            var vm = this;
            if (window.DeviceMotionEvent) {
                window.removeEventListener('devicemotion', vm.deviceMotionHandler, false);
            }
        }
        /*completeQuiz: function(quiz_index, question_index){
            var vm = this;
            //quizs -> question -> answer

            //DOM manipulate
            var elm = {
                currentQuestion : $('#popup-quiz-'+quiz_index+'-question-'+question_index),
                nextQuestion : $('#popup-quiz-'+quiz_index+'-question-'+(question_index+1))
            }
            elm.currentQuestion.trigger('close');
            if( elm.nextQuestion.length > 0 ){
                //go to next question
                elm.nextQuestion.lightbox_me();
            }else{
                //current quiz(all questions) completed
                if(this.userData.quizs[quiz_index].completed != true){ //only show once the user first complete the quiz
                    $("#popup-quiz-done").lightbox_me({
                        onClose: function(){
                            vm.popupCallback();
                        }
                    });
                    this.userData.quizs[quiz_index].completed = true;
                }
            }

            //viewModel
            if(this.userData.quizs[quiz_index].questions[question_index].completed != true){
                this.addUserPoints(POINTS.question);
            }
            this.userData.quizs[quiz_index].questions[question_index].completed = true;
        },*/
        /*addUserPoints: function(value){
            var vm = this;
            vm.userData.points = vm.userData.points + value;
        },
        addGamePoints: function(boothIndex){
            var vm = this;
            var _points;
            //check if completed the game
            if (vm.userData.booths[boothIndex].game.completed == true) {
                //completed before, no extra point
                _points = 0;
            } else {
                //first time, game point
                _points = vm.userData.booths[boothIndex].game.points
                vm.userData.booths[boothIndex].game.completed = true;
                $("#popup-camera").lightbox_me({
                    onClose: function(){
                        vm.popupCallback();
                    }
                })
            }
            //add point
            vm.addUserPoints(_points)
        },
        addCheckinPoints: function(newBeaconName){
            var vm = this;
            var arr = vm.userData.booths;
            var obj = arr.filter(booth => booth.name == newBeaconName)[0];
            var idx = arr.indexOf(obj);
            if(arr[idx].checkedIn == true){
                //checked in before
            }else{
                //first time checked in                
                vm.addUserPoints(arr[idx].checkInPoints);
                arr[idx].checkedIn = true;
                navigator.vibrate(1000);
                $('#popup-checkin').lightbox_me({
                    onClose: function(){
                        //scroll to the booth
                        var _id = "booth-"+idx;
                        var elm = document.getElementById(_id);
                        elm.scrollIntoView({
                            behavior: "smooth"
                        });
                        vm.popupCallback();
                    }
                });
            }
        }*/
    },
    beforeDestroy() {
        this.beaconsInterval = function(){};
        this.deactivateShakeFn();
    },
    mounted: function(){
        this.setUserInfo();
        this.manualBeaconsInterval();
        this.activateShakeFn();
    }
})

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


$("body").on('click', '.btn-lightbox', function(e){
    e.preventDefault();
    var id = $(this).attr('data-lightbox');
    $(id).lightbox_me()
})