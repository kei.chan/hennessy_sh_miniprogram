'use strict';
var express	    = require('express'),
    bodyparser  = require('body-parser'),
    app	        = express(),
    http	    = require('http'),
    https	    = require('https'),
    server	    = http.createServer(app),
    crypto      = require('crypto');

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Cache-Control", "max-age=1");
    next();
});
app.use(express.static(__dirname));

app.get("/test", function(req, res){
    res.send('Hello World 1');
});
app.post("/test", function(req, res){
    res.send('Hello World 2');
});

app.get("/getSignature", function(req, res){
    var url = req.query.url ;
    var appid = req.query.appid ;
    var appsecret = req.query.appsecret ;
    var preRes = res;
    var token_url = 'https://api.weixin.qq.com/cgi-bin/token';
    var request_token_path = token_url + "?grant_type=client_credential&appid="+appid+"&secret="+appsecret;
    console.log('request_token_path:', request_token_path)
    //request access token START
    https.get(request_token_path, function (res) {
        res.on('data', (d) => {
            d = JSON.parse(d.toString());
            var access_token = d.access_token;
            var request_ticket_path = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='+access_token+'&type=jsapi';
            console.log('request_ticket_path:', request_ticket_path)
            //request jsapi ticket START
            https.get(request_ticket_path, function (res) {
                res.on('data', function (d) {
                    var d = JSON.parse(d.toString());
                    var jsapi_ticket = d.ticket;

                    var noncestr =   Math.random().toString().substr(2);
                    var timestamp =  new Date().getTime();

                    var targetStr = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url="+url;
                    console.log('targetStr:', targetStr)

                    var sha1_result = crypto.createHash('sha1').update(targetStr).digest('hex');

                    preRes.json({
                        'signature': sha1_result,
                        'nonceStr': noncestr,
                        'timestamp': timestamp
                    })
                }).on('error', function(e){
                    console.log('Get Error in request jsapi')
                })
            });
        }).on('error', function(e){
            console.log('Get Error in request token')
        })
    });
});

server.listen(9000, function () {
    console.log('Server listening at port %d', 9000);
});
